<?php

/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2022 (original work) Open Assessment Technologies SA;
 *               
 * 
 */

namespace sayegh1944\taoTestTakerEnhance\controller;

use oat\generis\model\data\event\ResourceCreated;
use oat\oatbox\event\EventManager;

use core_kernel_classes_Resource;
use core_kernel_classes_Property;

use oat\generis\model\OntologyRdfs;
use oat\tao\model\TaoOntology;

/**
 * Sample controller
 *
 * @author Open Assessment Technologies SA
 * @package taoTestTakerEnhance
 * @license GPL-2.0
 *
 */
class TaoTestTakerEnhance extends \tao_actions_CommonModule
{

    /**
     * initialize the services
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * A possible entry point to tao
     */
    public function index()
    {
        $ResourceElement = new core_kernel_classes_Resource("http://tao-ext.local/tao.rdf#i166098268732034588");
        $propertyLabel = new core_kernel_classes_Property(RDF_TYPE);

        $ClassType = $ResourceElement->getPropertyValues($propertyLabel);

        $DataHere = $this->getParentResourceType($ClassType);
        var_export("<pre>" . $DataHere . "</pre>");
        die();

        echo __("Hello World");
    }
    public function getParentResourceType($ResourceID)
    {

        $ClassesTarget = [
            TaoOntology::CLASS_URI_ITEM,
            TaoOntology::CLASS_URI_GROUP,
            TaoOntology::CLASS_URI_SUBJECT,
            TaoOntology::CLASS_URI_TEST,
            TaoOntology::CLASS_URI_DELIVERY,
            TaoOntology::CLASS_URI_DELIVERY
        ];

        if (is_array($ResourceID)){
            if (!empty($ResourceID)) {
                if (is_array($ResourceID)){
                    $ResourceID = $ResourceID[0]; 
                }
            }
        };

        if (in_array($ResourceID, $ClassesTarget)) {
            return true;
        } else {
            if (is_string($ResourceID)) {
                $ResourceElement = new core_kernel_classes_Resource($ResourceID);
                $propertyLabel = new core_kernel_classes_Property(RDFS_SUBCLASSOF);

                $ClassType = $ResourceElement->getPropertyValues($propertyLabel);
                if($this->getParentResourceType($ClassType) == false){
                    return false;
                }else{
                    return true;
                }
                
            }
        }
    }

    public function templateExample()
    {
        $this->setData('author', 'Open Assessment Technologies SA');
        $this->setView('TaoTestTakerEnhance/templateExample.tpl');
    }
}
