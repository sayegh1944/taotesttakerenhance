<?php

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright (c) 2021 (original work) Open Assessment Technologies SA;
 */

namespace sayegh1944\taoTestTakerEnhance\scripts\install;

use oat\oatbox\extension\InstallAction;

use oat\generis\model\OntologyAwareTrait;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class DBScript extends InstallAction
{
    use OntologyAwareTrait;
    use ServiceLocatorAwareTrait;


    public function __invoke($params)
    {
        $persistence = $this->getModel()->getPersistence();

        $Query = "
        CREATE OR REPLACE VIEW `TestTakerViewData` AS
        SELECT DISTINCT `s1`.`subject` AS `TestTakerURI`,
            (
                select json_arrayagg(
                        json_object(
                            'label',
                            (
                                select `s3`.`object`
                                from `statements` `s3`
                                where (
                                        (
                                            `s3`.`predicate` = 'http://www.w3.org/2000/01/rdf-schema#label'
                                        )
                                        and (`s3`.`subject` = `s2`.`object`)
                                    )
                            ),
                            'uri',
                            `s2`.`object`,
                            'delivery',
                            (
                                select json_arrayagg(
                                        json_object(
                                            'label',
                                            (
                                                select `s5`.`object`
                                                from `statements` `s5`
                                                where (
                                                        (
                                                            `s5`.`predicate` = 'http://www.w3.org/2000/01/rdf-schema#label'
                                                        )
                                                        and (`s5`.`subject` = `s4`.`object`)
                                                    )
                                            ),
                                            'uri',
                                            `s4`.`object`,
                                            'DeliveryExecutionDelivery',
                                            (
                                                select json_arrayagg(
                                                        json_object(
                                                            'label',
                                                            (
                                                                select `s7`.`object`
                                                                from `statements` `s7`
                                                                where (
                                                                        (
                                                                            `s7`.`predicate` = 'http://www.w3.org/2000/01/rdf-schema#label'
                                                                        )
                                                                        and (`s7`.`subject` = `s6`.`subject`)
                                                                    )
                                                            ),
                                                            'uri',
                                                            `s6`.`subject`,
                                                            'TAO_DELIVERY_TAKABLE',
                                                            (
                                                                select (
                                                                        case
                                                                            when (
                                                                                `s8`.`object` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusActive'
                                                                            ) then true
                                                                            when (
                                                                                `s8`.`object` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStatusPaused'
                                                                            ) then true
                                                                            else false
                                                                        end
                                                                    ) AS `QuantityText`
                                                                from `statements` `s8`
                                                                where (
                                                                        (
                                                                            `s8`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#StatusOfDeliveryExecution'
                                                                        )
                                                                        and (`s8`.`subject` = `s6`.`subject`)
                                                                    )
                                                            ),
                                                            'description',
                                                            (
                                                                select date_format(
                                                                        from_unixtime(substring_index(`s9`.`object`, ' ', -(1))),
                                                                        'Started at %d/%m/%Y %h:%i:%s'
                                                                    )
                                                                from `statements` `s9`
                                                                where (
                                                                        (
                                                                            `s9`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionStart'
                                                                        )
                                                                        and (`s9`.`subject` = `s6`.`subject`)
                                                                    )
                                                            )
                                                        )
                                                    )
                                                from `statements` `s6`
                                                where (
                                                        (`s6`.`object` = `s4`.`object`)
                                                        and (
                                                            `s6`.`predicate` = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#DeliveryExecutionDelivery'
                                                        )
                                                    )
                                            )
                                        )
                                    )
                                from `statements` `s4`
                                where (
                                        (`s4`.`subject` = `s2`.`object`)
                                        and (
                                            `s4`.`predicate` = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#Deliveries'
                                        )
                                    )
                            )
                        )
                    )
                from `statements` `s2`
                where (
                        (`s2`.`subject` = `s1`.`subject`)
                        and (
                            `s2`.`predicate` = 'http://www.tao.lu/Ontologies/TAOGroup.rdf#member'
                        )
                    )
            ) AS `TestTakerData`
        FROM `statements` AS `s1`
        WHERE `s1`.`subject` in (
                select distinct `s0`.`subject`
                from `statements` `s0`
                where (
                        (
                            `s0`.`object` = 'http://www.tao.lu/Ontologies/TAOSubject.rdf#Subject'
                        )
                        AND (
                            `s0`.`predicate` = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
                        )
                    )
            );
        ";

        $persistence->exec($Query);




        $Query = "
        CREATE TABLE IF NOT EXISTS `impoted_data_from_to_table_user_data` (
            `TestTakerURI` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
            `TestTakerData` json DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
          
          ALTER TABLE impoted_data_from_to_table_user_data ADD INDEX (TestTakerURI);
          
        ";

        $persistence->exec($Query);
    }
}
