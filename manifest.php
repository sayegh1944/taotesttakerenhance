<?php

/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2022 (original work) Open Assessment Technologies SA;
 *               
 * 
 */

use oat\tao\model\user\TaoRoles;

/**
 * Generated using taoDevTools 3.4.0
 */
return array(
    'name' => 'taoTestTakerEnhance',
    'label' => 'taoTestTakerEnhance',
    'description' => '',
    'license' => 'GPL-2.0',
    'version' => '0.1',
    'author' => 'Ahmed Omran - Sayegh',
    'requires' => array(
        'generis' => '>=8.2.2',
        'tao' => '>=27.1.4',
        'taoResultServer' => '>=8.1.1',
        'taoOutcomeRds' => '>=5.0.1',
        'taoDelivery' => '>=12.4.2',
        'taoBackOffice' => '>=3.3.1',
        'taoTestTaker' => '>=5.1.1',
        'taoGroups' => '>=4.2.1',
        'taoItems' => '>=6.6.3',
        'taoTests' => '>=8.4.1',
        'taoQtiItem' => '>=18.7.8',
        'taoQtiTest' => '>=30.5.3',
        'taoDeliveryRdf' => '>=7.4.6',
        'taoOutcomeUi' => '>=7.5.3',
        'taoQtiTestPreviewer' => '>=1.0.0',
        'qtiItemPci' => '>=4.10.1',
        'funcAcl' => '>=5.3.1',
        'taoCe' => '>=5.4.1'
    ),
    'managementRole' => 'http://www.tao.lu/Ontologies/generis.rdf#taoTestTakerEnhanceManager',
    'acl' => array(
        array('grant', 'http://www.tao.lu/Ontologies/generis.rdf#taoTestTakerEnhanceManager', array('ext' => 'taoTestTakerEnhance'))
    ),
    'install' => array(
        'php' => [
            \sayegh1944\taoTestTakerEnhance\scripts\install\RegisterEvents::class,
            \sayegh1944\taoTestTakerEnhance\scripts\install\DBScript::class

            
        ]
    ),
    'uninstall' => array(
        'php' => [
            \sayegh1944\taoTestTakerEnhance\scripts\uninstall\UnregisterEvents::class,
            \sayegh1944\taoTestTakerEnhance\scripts\uninstall\DBScript::class
        ]
    ),
    'routes' => array(
        '/taoTestTakerEnhance' => 'sayegh1944\\taoTestTakerEnhance\\controller'
    ),
    'constants' => array(
        # views directory
        "DIR_VIEWS" => dirname(__FILE__) . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR,

        #BASE URL (usually the domain root)
        'BASE_URL' => ROOT_URL . 'taoTestTakerEnhance/',
    ),
    'extra' => array(
        'structures' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . 'structures.xml',
    )
);
